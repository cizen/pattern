defmodule Pattern.Dispatcher.Node do
  @moduledoc false

  alias Pattern
  alias Pattern.Code

  defstruct subscriptions: MapSet.new([]),
            operations: %{}

  @type t :: %__MODULE__{subscriptions: MapSet.t(), operations: %{Code.t() => %{term => t}}}

  @spec new :: t
  def new, do: %__MODULE__{}

  @spec put(t, Code.t(), any) :: t
  def put(node, code, subscription) do
    run(node, {:update, code, {:put_subscription, subscription}})
  end

  @spec delete(t, Code.t(), any) :: t
  def delete(node, code, subscription) do
    run(node, {:update, code, {:delete_subscription, subscription}})
  end

  @spec get(t, struct) :: MapSet.t()
  def get(node, struct) do
    %__MODULE__{subscriptions: subscriptions, operations: operations} = node

    Enum.reduce(operations, subscriptions, fn {operation, nodes}, subscriptions ->
      value = Pattern.eval(operation, struct)
      node = Map.get(nodes, value, new())
      MapSet.union(subscriptions, get(node, struct))
    end)
  end

  @type command ::
          {:update, Code.t(), command} | {:put_subscription | :delete_subscription, any}
  @spec run(t, command) :: t
  defp run(node, {:update, true, continuation}) do
    run(node, continuation)
  end

  defp run(node, {:update, {:==, [operation, value]}, continuation})
       when not is_tuple(value) or tuple_size(value) != 2 do
    update_operation(node, operation, value, continuation)
  end

  defp run(node, {:update, {:==, [value, operation]}, continuation})
       when not is_tuple(value) or tuple_size(value) != 2 do
    update_operation(node, operation, value, continuation)
  end

  defp run(node, {:update, {:not, [operation]}, continuation}) do
    update_operation(node, operation, false, continuation)
  end

  defp run(node, {:update, {:and, [left, right]}, continuation}) do
    run(node, {:update, left, {:update, right, continuation}})
  end

  defp run(node, {:update, {:or, [left, right]}, continuation}) do
    node
    |> run({:update, left, continuation})
    |> run({:update, right, continuation})
  end

  defp run(node, {:update, operation, continuation}) do
    update_operation(node, operation, true, continuation)
  end

  defp run(node, {:put_subscription, subscription}) do
    update_in(node.subscriptions, &MapSet.put(&1, subscription))
  end

  defp run(node, {:delete_subscription, subscription}) do
    update_in(node.subscriptions, &MapSet.delete(&1, subscription))
  end

  defp update_operation(node, operation, value, continuation) do
    values = Map.get(node.operations, operation, %{})

    next_node =
      values
      |> Map.get(value, new())
      |> run(continuation)

    node = put_in(node.operations[operation], values)
    put_in(node.operations[operation][value], next_node)
  end
end
